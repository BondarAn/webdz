from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
	user = models.CharField(max_length = 50)
	password = models.CharField(max_length = 40, null = True)
	email = models.EmailField(null = True)
	rating = models.IntegerField(default = 0)
	avatar_url = models.CharField(max_length = 60)
	reg_date = models.DateTimeField(auto_now_add = True, null = True)

class Question(models.Model):
	author = models.ForeignKey(Profile)
	title = models.CharField(max_length = 80)
	text = models.TextField()
	date_added = models.DateTimeField(auto_now_add = True)

class Anwser(models.Model):
	author = models.ForeignKey(Profile)
	question = models.ForeignKey(Question)
	text = models.TextField()
	date_added = models.DateTimeField(auto_now_add = True)
	is_right = models.BooleanField(default = False)

class Tag(models.Model):
	name = models.CharField(max_length = 20)
	weight = models.IntegerField()

