from faker.frandom import random
from mixer.fakers import get_username, get_email, get_lorem
import csv
import random

table = []
outfile = open('UserNames.csv', 'w')
writer = csv.writer(outfile)
date = "2014-11-05"
for i in range(0, 5000):
	name = get_username()
	email = get_email()
	writer.writerow([name, "", email, 0, ""])

outfile.close()

outfile = open('Questions.csv', 'w')
writer = csv.writer(outfile)
for i in range(1, 50000):
	title = get_lorem(length = 50)
	text = get_lorem(length = 250)
	author = random.randint(1, 5000)
	writer.writerow([author,title,text])
outfile.close()

outfile = open('Anwsers.csv', 'w')
writer = csv.writer(outfile)
for i in xrange(1, 250000):
	title = get_lorem(length = 25)
	text = get_lorem(length = 150)
	author = random.randint(1, 5000)
	writer.writerow([author,title,text])
outfile.close()

outfile = open('Tags.csv', 'w')
writer = csv.writer(outfile)
for i in range(1, 100):
	name = get_lorem(length = 15)
	weight = random.randint(1, 10)
	writer.writerow([name, weight])
outfile.close()




