
# coding=utf-8
from urlparse import parse_qs
 
def application(environ, start_response):
    status = '200 OK'
 
    output = '''
	    Hello World	<br>
            <meta charset="utf8">
            GET параметры:
    '''
   
    GET = parse_qs(str(environ.get('QUERY_STRING')))
    for key in GET:
        output += key + " => " + GET[key][0] + ", "
 
    output += "<br>"
 
    output += "POST параметры: "
 
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0
 
    request_body = environ['wsgi.input'].read(request_body_size)
    query_list = parse_qs(request_body)
 
    for key in query_list:
        output += key + " => " + query_list[key][0] + ", "
 
    response_headers = [('Content-type', 'text/html'), ("Content-length", str(len(output)))]
    start_response(status, response_headers)
    return [output]
