from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from cgi import parse_qs
 
 
def get_post_para(request):
        status = '200 OK'
 
        output = '<html><body>HELLo World! <br>'
 
        GET = request.GET
 
        output += "GET params: <br>"
        for key in GET:
                output += "  KEY = "+key+"; VAL = " + GET[key]+ "; <br>"
        POST = request.POST
        output += "POST params: <br>"
        for key in POST:
                output += "  KEY = "+key+"; VAL = " + POST[key]+ "; <br>"
        output += "</body></html>"
        return HttpResponse([output])

def getIndex(request):
	return render_to_response("index.html", {"foo" : "bar"});

def getLogin(request):
	return render_to_response("login.html", {"foo" : "bar"});

def getSingUp(request):
	return render_to_response("singup.html", {"foo" : "bar"});