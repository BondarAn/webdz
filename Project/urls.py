from django.conf.urls import patterns, include, url
from django.contrib import admin
from polls import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^$', 'Project.views.get_post_para'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', "Project.views.getIndex"),
    url(r'^login/', "Project.views.getLogin"),
    url(r'^singup/', "Project.views.getSingUp"),
)
